<?php

$EM_CONF[$_EXTKEY] = [
	'title' => 'Scheduled Social',
	'description' => 'Post news articles to social media platforms and fetch likes from your social media account',
	'category' => 'plugin',
	'version' => '2.0.0',
	'state' => 'stable',
	'uploadfolder' => 0,
	'author' => 'Johannes Kasberger',
	'author_email' => 'office@reelworx.at',
	'author_company' => 'Reelworx GmbH',
	'constraints' => [
		'depends' => [
			'typo3' => '8.7.0-9.5.99',
            'scheduler' => '8.7.0-9.5.99',
            'news' => '5.0.0-7.99.99',
        ],
		'conflicts' => [],
		'suggests' => [],
    ],
];
