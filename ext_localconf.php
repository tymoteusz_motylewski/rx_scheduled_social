<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
	function($extKey)
	{
        $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['scheduler']['tasks'][\Reelworx\RxScheduledSocial\Scheduler\FacebookFetchLikesTask::class] = array(
            'extension' => $extKey,
            'title' => 'Fetch likes of facebook page',
            'description' => 'Fetch likes of facebook page',
            'additionalFields' => \Reelworx\RxScheduledSocial\Scheduler\FacebookFetchLikesAdditionalFieldsProvider::class
        );

        $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['scheduler']['tasks'][\Reelworx\RxScheduledSocial\Scheduler\TwitterPublishTask::class] = array(
            'extension' => $extKey,
            'title' => 'Publish news to Twitter',
            'description' => 'Publish new news posts to Twitter',
            'additionalFields' => \Reelworx\RxScheduledSocial\Scheduler\TwitterPublishAdditionalFieldsProvider::class
        );

        $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['scheduler']['tasks'][\Reelworx\RxScheduledSocial\Scheduler\TwitterFetchFollowersTask::class] = array(
            'extension' => $extKey,
            'title' => 'Fetch follower count of twitter user',
            'description' => 'Fetches the follower count of a twitter user',
            'additionalFields' => \Reelworx\RxScheduledSocial\Scheduler\TwitterFetchFollowersAdditionalFieldsProvider::class
        );

        // register callback to set the hostname for realurl when called via the cli
        $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['ext/realurl/class.tx_realurl_autoconfgen.php']['postProcessConfiguration']['social'] = \Reelworx\RxScheduledSocial\Utility\LinkUtility::class . '->registerHostHook';
	},
	$_EXTKEY
);
