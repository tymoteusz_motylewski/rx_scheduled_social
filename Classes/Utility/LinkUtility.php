<?php
declare(strict_types=1);

/*
 *
 * This file is part of the rx_scheduled_social Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * Copyright (c) Reelworx GmbH
 *
 */

namespace Reelworx\RxScheduledSocial\Utility;

use TYPO3\CMS\Core\Page\PageRenderer;
use TYPO3\CMS\Frontend\Authentication\FrontendUserAuthentication;
use TYPO3\CMS\Frontend\Controller\TypoScriptFrontendController;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class LinkUtility
{
    public static function link(int $pageUid, string $recordLinkId, int $recordUid) : string
    {
        if (!$pageUid) {
            throw new \InvalidArgumentException('You must specify a news detail page id.');
        }
        // use linkhandler to generate the link to the news post
        $config = [
            'parameter' => 't3://record?identifier='. $recordLinkId . '&uid=' . $recordUid,
            'forceAbsoluteUrl' => '1',
            'useCacheHash' => '1',
        ];

        if (version_compare(TYPO3_branch, '8.7', '=')) {
            self::buildFakeFEv8($pageUid);
        } else {
            self::buildFakeFEv9($pageUid);
        }
        return $GLOBALS['TSFE']->cObj->typoLink_URL($config);
    }

    private static function buildFakeFEv9(int $pageUid) : bool
    {
        if (isset($GLOBALS['TSFE']) || !$pageUid) {
            return false;
        }
        $pageRendererBackup = GeneralUtility::makeInstance(PageRenderer::class);
        $instances = GeneralUtility::getSingletonInstances();
        unset($instances[PageRenderer::class]);
        GeneralUtility::resetSingletonInstances($instances);

        // simulate a normal FE without any logged-in FE or BE user
        // enable search to root page
        $GLOBALS['TYPO3_CONF_VARS']['FE']['pageNotFound_handling'] = '';
        // $TYPO3_CONF_VARS, $id, $type, $no_cache = '', $cHash = '', $jumpurl = '', $MP = '', $RDCT = ''
        /** @var TypoScriptFrontendController $tsfe */
        $tsfe = GeneralUtility::makeInstance(TypoScriptFrontendController::class, null, $pageUid, 0);
        $GLOBALS['TSFE'] = $tsfe;

        try {
            $frontendUser = GeneralUtility::makeInstance(FrontendUserAuthentication::class);
            $frontendUser->start();
            $frontendUser->unpack_uc();
            // Keep the backwards-compatibility for TYPO3 v9, to have the fe_user within the global TSFE object
            $GLOBALS['TSFE']->fe_user = $frontendUser;

            $tsfe->clear_preview();
            $tsfe->determineId();
            $tsfe->getFromCache();
            $tsfe->getConfigArray();
        } catch (\Exception $e) {
            return false;
        }

        // calculate the absolute path prefix
        if (!empty($tsfe->config['config']['socialDomain'])) {
            $absRefPrefix = trim($tsfe->config['config']['socialDomain']);
            if ($absRefPrefix === 'auto') {
                $tsfe->absRefPrefix = GeneralUtility::getIndpEnv('TYPO3_SITE_PATH');
            } else {
                $tsfe->absRefPrefix = $absRefPrefix;
            }
        } else {
            $tsfe->absRefPrefix = '';
        }
        $tsfe->newCObj();

        GeneralUtility::setSingletonInstance(PageRenderer::class, $pageRendererBackup);

        return true;
    }

    private static function buildFakeFEv8(int $pageUid) : bool
    {
        if (isset($GLOBALS['TSFE']) || !$pageUid) {
            return false;
        }
        $pageRendererBackup = GeneralUtility::makeInstance(PageRenderer::class);
        $instances = GeneralUtility::getSingletonInstances();
        unset($instances[PageRenderer::class]);
        GeneralUtility::resetSingletonInstances($instances);

        // simulate a normal FE without any logged-in FE or BE user
        // enable search to root page
        $GLOBALS['TYPO3_CONF_VARS']['FE']['pageNotFound_handling'] = '';
        // $TYPO3_CONF_VARS, $id, $type, $no_cache = '', $cHash = '', $jumpurl = '', $MP = '', $RDCT = ''
        /** @var $fe TypoScriptFrontendController */
        $tsfe = GeneralUtility::makeInstance(TypoScriptFrontendController::class, null, $pageUid, 0);
        $GLOBALS['TSFE'] = $tsfe;
        $tsfe->beUserLogin = false;
        try {
            $tsfe->initFEuser();
            $tsfe->clear_preview();
            $tsfe->determineId();
            $tsfe->makeCacheHash();
            $tsfe->initTemplate();
            $tsfe->getFromCache();
            $tsfe->getConfigArray();
        } catch (\Exception $e) {
            return false;
        }

        // calculate the absolute path prefix
        if (!empty($tsfe->config['config']['socialDomain'])) {
            $absRefPrefix = trim($tsfe->config['config']['socialDomain']);
            if ($absRefPrefix === 'auto') {
                $tsfe->absRefPrefix = GeneralUtility::getIndpEnv('TYPO3_SITE_PATH');
            } else {
                $tsfe->absRefPrefix = $absRefPrefix;
            }
        } else {
            $tsfe->absRefPrefix = '';
        }
        $tsfe->newCObj();

        GeneralUtility::setSingletonInstance(PageRenderer::class, $pageRendererBackup);

        return true;
    }

    /**
     * Set the correct hostname when called via cli
     *
     * @param array $params
     * @return string
     */
    public function getHost(array $params): string
    {
        if ($params['host'] === '') {
            $domain = $GLOBALS['TSFE']->absRefPrefix;
            if ($domain) {
                return parse_url($domain)['host'];
            }
        }
        return '';
    }

    /**
     * add another hook to the configuration that sets the hostname
     *
     * @param array $params
     */
    public function registerHostHook(array &$params)
    {
        $params['config']['getHost']['social'] = self::class . '->getHost';
    }
}
