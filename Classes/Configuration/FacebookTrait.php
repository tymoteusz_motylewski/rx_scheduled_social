<?php
/*
 *
 * This file is part of the rx_scheduled_social Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * Copyright (c) Reelworx GmbH
 *
 */

namespace Reelworx\RxScheduledSocial\Configuration;


trait FacebookTrait
{
    /** @var string */
    private $appId;

    /** @var string */
    private $appSecret;

    /** @var string */
    private $appToken;


    protected function isFacebookConfigurationValid()
    {
        return !empty($this->appId) && !empty($this->appSecret) && !empty($this->appToken);
    }

    /**
     * @return string
     */
    public function getAppId(): string
    {
        return $this->appId;
    }

    /**
     * @param string $appId
     */
    public function setAppId(string $appId)
    {
        $this->appId = $appId;
    }

    /**
     * @return string
     */
    public function getAppSecret(): string
    {
        return $this->appSecret;
    }

    /**
     * @param string $appSecret
     */
    public function setAppSecret(string $appSecret)
    {
        $this->appSecret = $appSecret;
    }

    /**
     * @return string
     */
    public function getAppToken(): string
    {
        return $this->appToken;
    }

    /**
     * @param string $appToken
     */
    public function setAppToken(string $appToken)
    {
        $this->appToken = $appToken;
    }
}
