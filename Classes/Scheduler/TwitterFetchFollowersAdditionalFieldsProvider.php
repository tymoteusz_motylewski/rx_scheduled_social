<?php
declare(strict_types=1);

/*
 *
 * This file is part of the rx_scheduled_social Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * Copyright (c) Reelworx GmbH
 *
 */

namespace Reelworx\RxScheduledSocial\Scheduler;

use TYPO3\CMS\Core\Exception;
use TYPO3\CMS\Core\Messaging\FlashMessage;
use TYPO3\CMS\Core\Messaging\FlashMessageService;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Scheduler\AdditionalFieldProviderInterface;
use TYPO3\CMS\Scheduler\Controller\SchedulerModuleController;
use TYPO3\CMS\Scheduler\Task\AbstractTask;


class TwitterFetchFollowersAdditionalFieldsProvider implements AdditionalFieldProviderInterface
{

    /**
     * gets additional fields
     *
     * @param array $taskInfo
     * @param AbstractTask $task
     * @param SchedulerModuleController $schedulerModule
     * @return array A two dimensional array
     */
    public function getAdditionalFields(array &$taskInfo, $task, SchedulerModuleController $schedulerModule)
    {
        /** @var TwitterFetchFollowersTask $task */
        $additionalFields = [];

        $appId = '';
        $appToken = '';
        $appSecret = '';
        $appTokenSecret = '';
        $screenName = '';

        if ($task) {
            $appId = $task->getAppId();
            $appSecret = $task->getAppSecret();
            $appToken = $task->getAppToken();
            $screenName = $task->getScreenName();
            $appTokenSecret = $task->getAppTokenSecret();
        }

        $additionalFields['appId'] = [
            'code' => '<input class="form-control" type="text" name="tx_scheduler[tff][appId]" value="'
                      . htmlspecialchars($appId) . '" />',
            'label' => 'LLL:EXT:rx_scheduled_social/Resources/Private/Language/locallang.xlf:twitter.appid',
            'cshKey' => '',
            'cshLabel' => '',
        ];

        $additionalFields['appSecret'] = [
            'code' => '<input class="form-control" type="text" name="tx_scheduler[tff][appSecret]" value="'
                      . htmlspecialchars($appSecret) . '" />',
            'label' => 'LLL:EXT:rx_scheduled_social/Resources/Private/Language/locallang.xlf:twitter.appsecret',
            'cshKey' => '',
            'cshLabel' => '',
        ];

        $additionalFields['appToken'] = [
            'code' => '<input class="form-control" type="text" name="tx_scheduler[tff][appToken]" value="'
                      . htmlspecialchars($appToken) . '" />',
            'label' => 'LLL:EXT:rx_scheduled_social/Resources/Private/Language/locallang.xlf:twitter.apptoken',
            'cshKey' => '',
            'cshLabel' => '',
        ];

        $additionalFields['appTokenSecret'] = [
            'code' => '<input class="form-control" type="text" name="tx_scheduler[tff][appTokenSecret]" value="'
                      . htmlspecialchars($appTokenSecret) . '" />',
            'label' => 'LLL:EXT:rx_scheduled_social/Resources/Private/Language/locallang.xlf:twitter.apptokensecret',
            'cshKey' => '',
            'cshLabel' => '',
        ];

        $additionalFields['screenName'] = [
            'code' => '<input class="form-control" type="text" name="tx_scheduler[tff][screenName]" value="'
                      . htmlspecialchars($screenName) . '" />',
            'label' => 'LLL:EXT:rx_scheduled_social/Resources/Private/Language/locallang.xlf:twitter.screenName',
            'cshKey' => '',
            'cshLabel' => '',
        ];

        return $additionalFields;
    }

    /**
     * Validates the additional fields' values
     *
     * @param array $submittedData
     * @param SchedulerModuleController $schedulerModule
     * @return boolean TRUE if validation was ok, FALSE otherwise
     * @throws Exception
     */
    public function validateAdditionalFields(array &$submittedData, SchedulerModuleController $schedulerModule)
    {
        $errors = [];

        $data = $submittedData['tff'];

        if (trim($data['appId']) === '') {
            $errors[] = 'scheduler.error.appid_isempty';
        }

        if (trim($data['appSecret']) === '') {
            $errors[] = 'scheduler.error.appsecret_isempty';
        }

        if (trim($data['appToken']) === '') {
            $errors[] = 'scheduler.error.apptoken_isempty';
        }

        if (trim($data['appTokenSecret']) === '') {
            $errors[] = 'scheduler.error.apptokensecret_isempty';
        }

        if (trim($data['screenName']) === '') {
            $errors[] = 'scheduler.error.screenName_invalid';
        }

        foreach ($errors as $error) {
            /** @noinspection PhpUndefinedMethodInspection */
            $error = $GLOBALS['LANG']->sL('LLL:EXT:rx_scheduled_social/Resources/Private/Language/locallang.xlf:'
                                          . $error);
            $this->addErrorMessage($error);
        }

        return count($errors) == 0;
    }

    /**
     * set values in the task object
     *
     * @param array $submittedData
     * @param AbstractTask $task
     * @return void
     */
    public function saveAdditionalFields(array $submittedData, AbstractTask $task)
    {
        $data = $submittedData['tff'];

        /** @var TwitterFetchFollowersTask $task */
        $task->setAppSecret($data['appSecret']);
        $task->setAppId($data['appId']);
        $task->setAppToken($data['appToken']);
        $task->setScreenName($data['screenName']);
        $task->setAppTokenSecret($data['appTokenSecret']);
    }

    /**
     * Adds a error message as a flash message.
     *
     * @param string $message
     * @return void
     * @throws Exception
     */
    protected function addErrorMessage($message)
    {
        $flashMessage = GeneralUtility::makeInstance(FlashMessage::class, $message, '', FlashMessage::ERROR);
        /** @var FlashMessage $flashMessage */
        $flashMessageService = GeneralUtility::makeInstance(FlashMessageService::class);
        /** @var FlashMessageService $flashMessageService */
        $flashMessageService->getMessageQueueByIdentifier()->enqueue($flashMessage);
    }
}
