<?php
declare(strict_types=1);

/*
 *
 * This file is part of the rx_scheduled_social Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * Copyright (c) Reelworx GmbH
 *
 */

namespace Reelworx\RxScheduledSocial\Scheduler;

use Facebook\Exceptions\FacebookResponseException;
use Facebook\Exceptions\FacebookSDKException;
use Facebook\Facebook;
use Reelworx\RxScheduledSocial\Configuration\FacebookTrait;
use TYPO3\CMS\Core\Registry;
use TYPO3\CMS\Core\Utility\GeneralUtility;

require_once __DIR__ . '/../../Resources/Private/Facebook/vendor/autoload.php';

class FacebookFetchLikesTask extends AbstractTask
{
    use FacebookTrait;

    /** @var int */
    private $facebookPageId;

    /**
     * Runs the task
     */
    public function execute()
    {
        $registry = GeneralUtility::makeInstance(Registry::class);

        if (!$this->isFacebookConfigurationValid() || empty($this->facebookPageId)) {
            $this->getLogger()->error('facebook fetch fan count setup incomplete');
            return false;
        }

        $fb = null;
        try {
            $fb = new Facebook([
                'app_id' => $this->appId,
                'app_secret' => $this->appSecret,
                'default_graph_version' => 'v2.7',
            ]);
        } catch (FacebookSDKException $e) {
            $this->getLogger()->error('Facebook SDK returned an error: ' . $e->getMessage());
            return false;
        }

        try {
            $response = $fb->get('/' . $this->facebookPageId . '?fields=fan_count', $this->appToken);
        } catch (FacebookResponseException $e) {
            $this->getLogger()->alert('Graph returned an error: ' . $e->getMessage());
            return false;
        } catch (FacebookSDKException $e) {
            $this->getLogger()->alert('Facebook SDK returned an error: ' . $e->getMessage());
            return false;
        }

        try {
            $fanCount = $response->getGraphPage()->getField('fan_count');
        } catch (FacebookSDKException $e) {
            $this->getLogger()->alert('Facebook SDK returned an error: ' . $e->getMessage());
            return false;
        }

        $registry->set('rx_scheduled_social', 'facebook_' . $this->facebookPageId . '.fan_count', $fanCount);

        return true;
    }

    /**
     * @return int
     */
    public function getFacebookPageId(): int
    {
        return $this->facebookPageId;
    }

    /**
     * @param int $facebookPageId
     */
    public function setFacebookPageId(int $facebookPageId)
    {
        $this->facebookPageId = $facebookPageId;
    }
}
