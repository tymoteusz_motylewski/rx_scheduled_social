<?php
declare(strict_types=1);

/*
 *
 * This file is part of the rx_scheduled_social Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * Copyright (c) Reelworx GmbH
 *
 */

namespace Reelworx\RxScheduledSocial\Scheduler;

use Codebird\Codebird;
use Reelworx\RxScheduledSocial\Configuration\TwitterTrait;
use TYPO3\CMS\Core\Utility\GeneralUtility;


require_once __DIR__ . '/../../Resources/Private/Twitter/vendor/autoload.php';

class TwitterPublishTask extends AbstractNewsTask
{
    use TwitterTrait;

    /**
     * Runs the task
     */
    public function execute()
    {
        if (!$this->isTwitterConfigurationValid() || !$this->isNewsConfigurationValid()) {
            $this->getLogger()->error('twitter publish setup incomplete');
            return false;
        }

        Codebird::setConsumerKey($this->appId, $this->appSecret);
        $cb = Codebird::getInstance();
        $cb->setToken($this->appToken, $this->appTokenSecret);

        return $this->fetchNews('twitter_published', function ($postData) use ($cb) {
            $status = $postData['title'];
            // URL always counted as 22 strlen, 1 space
            $maxLength = 140 - 24;
            if (strlen($status) > $maxLength) {
                $status = GeneralUtility::fixed_lgd_cs($status, $maxLength - 3);
            } // -3 for dots that are appended

            $status .= ' ' . $postData['link'];
            $params = [
                'status' => $status,
            ];

            $reply = null;
            try {
                /** @noinspection PhpUndefinedMethodInspection */
                $reply = $cb->statuses_update($params);
            } catch (\Exception $e) {
                $this->getLogger()->error('Twitter return an error: ' . $e->getMessage());
                return false;
            }

            if (!isset($reply->id)) {
                $this->getLogger()->error('Twitter return an error: ' . print_r($reply, true));
                return false;
            }
            return true;
        });
    }
}
