<?php
declare(strict_types=1);

/*
 *
 * This file is part of the rx_scheduled_social Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * Copyright (c) Reelworx GmbH
 *
 */

namespace Reelworx\RxScheduledSocial\Scheduler;

use Codebird\Codebird;
use Reelworx\RxScheduledSocial\Configuration\TwitterTrait;
use TYPO3\CMS\Core\Registry;
use TYPO3\CMS\Core\Utility\GeneralUtility;

require_once __DIR__ . '/../../Resources/Private/Twitter/vendor/autoload.php';

class TwitterFetchFollowersTask extends AbstractTask
{
    use TwitterTrait;

    /** @var string */
    private $screenName;

    /**
     * Runs the task
     */
    public function execute()
    {
        $registry = GeneralUtility::makeInstance(Registry::class);

        if (!$this->isTwitterConfigurationValid() || empty($this->screenName)) {
            $this->getLogger()->error('twitter fetch follower setup incomplete');
            return false;
        }
        Codebird::setConsumerKey($this->appId, $this->appSecret);
        $cb = Codebird::getInstance();
        $cb->setToken($this->appToken, $this->appTokenSecret);

        /** @noinspection PhpUndefinedMethodInspection */
        $reply = $cb->users_show('screen_name=' . $this->screenName);
        $registry->set('rx_scheduled_social', 'twitter_' . $this->screenName . '.follower_count',
            $reply->followers_count);

        return true;
    }

    /**
     * @return string
     */
    public function getScreenName(): string
    {
        return $this->screenName;
    }

    /**
     * @param string $screenName
     */
    public function setScreenName(string $screenName)
    {
        $this->screenName = $screenName;
    }
}
