<?php
declare(strict_types=1);

/*
 *
 * This file is part of the rx_scheduled_social Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * Copyright (c) Reelworx GmbH
 *
 */

namespace Reelworx\RxScheduledSocial\Scheduler;

use TYPO3\CMS\Core\Exception;
use TYPO3\CMS\Core\Messaging\FlashMessage;
use TYPO3\CMS\Core\Messaging\FlashMessageService;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Scheduler\AdditionalFieldProviderInterface;
use TYPO3\CMS\Scheduler\Controller\SchedulerModuleController;
use TYPO3\CMS\Scheduler\Task\AbstractTask;


class FacebookFetchLikesAdditionalFieldsProvider implements AdditionalFieldProviderInterface
{

    /**
     * gets additional fields
     *
     * @param array $taskInfo
     * @param AbstractTask $task
     * @param SchedulerModuleController $schedulerModule
     * @return array A two dimensional array
     */
    public function getAdditionalFields(array &$taskInfo, $task, SchedulerModuleController $schedulerModule)
    {
        /** @var FacebookFetchLikesTask $task */
        $additionalFields = [];

        $appId = '';
        $appToken = '';
        $appSecret = '';
        $facebookPageId = 0;

        if ($task) {
            $appId = $task->getAppId();
            $appSecret = $task->getAppSecret();
            $appToken = $task->getAppToken();
            $facebookPageId = $task->getFacebookPageId();
        }

        $additionalFields['appId'] = [
            'code' => '<input class="form-control" type="text" name="tx_scheduler[fbl][appId]" value="'
                      . htmlspecialchars($appId) . '" />',
            'label' => 'LLL:EXT:rx_scheduled_social/Resources/Private/Language/locallang.xlf:facebook.appid',
            'cshKey' => '',
            'cshLabel' => '',
        ];

        $additionalFields['appSecret'] = [
            'code' => '<input class="form-control" type="text" name="tx_scheduler[fbl][appSecret]" value="'
                      . htmlspecialchars($appSecret) . '" />',
            'label' => 'LLL:EXT:rx_scheduled_social/Resources/Private/Language/locallang.xlf:facebook.appsecret',
            'cshKey' => '',
            'cshLabel' => '',
        ];

        $additionalFields['appToken'] = [
            'code' => '<input class="form-control" type="text" name="tx_scheduler[fbl][appToken]" value="'
                      . htmlspecialchars($appToken) . '" />',
            'label' => 'LLL:EXT:rx_scheduled_social/Resources/Private/Language/locallang.xlf:facebook.apptoken',
            'cshKey' => '',
            'cshLabel' => '',
        ];

        $additionalFields['fb_pid'] = [
            'code' => '<input class="form-control" type="text" name="tx_scheduler[fbl][fb_pid]" value="'
                      . htmlspecialchars($facebookPageId) . '" />',
            'label' => 'LLL:EXT:rx_scheduled_social/Resources/Private/Language/locallang.xlf:fb_pid',
            'cshKey' => '',
            'cshLabel' => '',
        ];

        return $additionalFields;
    }

    /**
     * Validates the additional fields' values
     *
     * @param array $submittedData
     * @param SchedulerModuleController $schedulerModule
     * @return boolean TRUE if validation was ok, FALSE otherwise
     * @throws Exception
     */
    public function validateAdditionalFields(array &$submittedData, SchedulerModuleController $schedulerModule)
    {
        $errors = [];

        $data = $submittedData['fbl'];
        if (trim($data['appId']) === '') {
            $errors[] = 'scheduler.error.appid_isempty';
        }

        if (trim($data['appSecret']) === '') {
            $errors[] = 'scheduler.error.appsecret_isempty';
        }

        if (trim($data['appToken']) === '') {
            $errors[] = 'scheduler.error.apptoken_isempty';
        }

        if ((int)$data['fb_pid'] < 0) {
            $errors[] = 'scheduler.error.fb_pid_invalid';
        }

        foreach ($errors as $error) {
            /** @noinspection PhpUndefinedMethodInspection */
            $error = $GLOBALS['LANG']->sL('LLL:EXT:rx_scheduled_social/Resources/Private/Language/locallang.xlf:'
                                          . $error);
            $this->addErrorMessage($error);
        }

        return count($errors) == 0;
    }

    /**
     * set values in the task object
     *
     * @param array $submittedData
     * @param AbstractTask $task
     * @return void
     */
    public function saveAdditionalFields(array $submittedData, AbstractTask $task)
    {
        $data = $submittedData['fbl'];

        /** @var FacebookFetchLikesTask $task */
        $task->setAppId($data['appId']);
        $task->setAppSecret($data['appSecret']);
        $task->setAppToken($data['appToken']);
        $task->setFacebookPageId($data['fb_pid']);
    }

    /**
     * Adds a error message as a flash message.
     *
     * @param string $message
     * @return void
     * @throws Exception
     */
    protected function addErrorMessage($message)
    {
        $flashMessage = GeneralUtility::makeInstance(FlashMessage::class, $message, '', FlashMessage::ERROR);
        /** @var FlashMessage $flashMessage */
        $flashMessageService = GeneralUtility::makeInstance(FlashMessageService::class);
        /** @var FlashMessageService $flashMessageService */
        $flashMessageService->getMessageQueueByIdentifier()->enqueue($flashMessage);
    }
}
