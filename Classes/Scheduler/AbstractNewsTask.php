<?php
declare(strict_types=1);

/*
 *
 * This file is part of the rx_scheduled_social Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * Copyright (c) Reelworx GmbH
 *
 */

namespace Reelworx\RxScheduledSocial\Scheduler;

use Reelworx\RxScheduledSocial\Utility\LinkUtility;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use GeorgRinger\News\Service\CategoryService;
use GeorgRinger\News\Utility\Page;

abstract class AbstractNewsTask extends AbstractTask
{
    /** @var int */
    protected $detailPid;

    /** @var  string */
    protected $pidList;

    /** @var  string */
    protected $categoryList;

    /** @var string */
    protected $recordLinkId = '';

    protected function isNewsConfigurationValid()
    {
        return $this->detailPid !== 0;
    }

    protected function fetchNews($postedFlag, callable $doPostAction)
    {
        $qb = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('tx_news_domain_model_news');
        $qb->select('n.uid', 'n.teaser', 'n.title')
            ->from('tx_news_domain_model_news', 'n')
            ->where($qb->expr()->eq('n.' . $postedFlag, 0));

        if (!empty($this->pidList)) {
            $pidList = Page::extendPidListByChildren($this->pidList, 5);
            if ($pidList !== '') {
                $qb->andWhere($qb->expr()->in('n.pid', $qb->createNamedParameter($pidList, \PDO::PARAM_STR)));
            }
        }

        if (!empty($this->categoryList)) {
            $categories = CategoryService::getChildrenCategories($this->categoryList);
            if ($categories !== '') {
                $qb->join('n', 'sys_category_record_mm', 'c', $qb->expr()->eq('c.uid_foreign', 'n.uid'));
                $qb->andWhere($qb->expr()->eq('c.tablenames', $qb->createNamedParameter('tx_news_domain_model_news', \PDO::PARAM_STR)));
                $qb->andWhere($qb->expr()->in('n.uid_local', $qb->createNamedParameter($categories, \PDO::PARAM_STR)));
            }
        }

        $res = $qb->execute();
        if (!$res) {
            $this->getLogger()->alert('Fetching of news posts failed with ' . $res->errorCode());
            return false;
        }

        $hasErrors = false;
        $updateBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('tx_news_domain_model_news');
        while ($row = $res->fetch()) {
            $postData = [
                'link' => LinkUtility::link($this->detailPid, $this->recordLinkId, (int)$row['uid']),
                'message' => trim($row['teaser']),
                'title' => trim($row['title']),
            ];

            if ($doPostAction($postData)) {
                $updateBuilder->update('tx_news_domain_model_news')
                    ->set($postedFlag, 1)
                    ->where($updateBuilder->expr()->eq('uid', $updateBuilder->createNamedParameter((int)$row['uid'], \PDO::PARAM_INT)));
                if (!$updateBuilder->execute()) {
                    $this->getLogger()->alert('Update of ' . $row['uid'] . ' failed with ' . $updateBuilder->getConnection()->errorCode());
                }
            } else {
                $this->getLogger()->alert('Publishing of news uid ' . $row['uid'] . ' failed', $postData);
                $hasErrors = true;
            }

        }
        $res->closeCursor();
        return !$hasErrors;
    }

    /**
     * @return int
     */
    public function getDetailPid(): int
    {
        return $this->detailPid;
    }

    /**
     * @param int $detailPid
     */
    public function setDetailPid(int $detailPid)
    {
        $this->detailPid = $detailPid;
    }

    /**
     * @return string
     */
    public function getPidList(): string
    {
        return $this->pidList;
    }

    /**
     * @param string $pidList
     */
    public function setPidList(string $pidList)
    {
        $this->pidList = $pidList;
    }

    /**
     * @return string
     */
    public function getCategoryList(): string
    {
        return $this->categoryList;
    }

    /**
     * @param string $categoryList
     */
    public function setCategoryList(string $categoryList)
    {
        $this->categoryList = $categoryList;
    }

    /**
     * @return string
     */
    public function getRecordLinkId(): string
    {
        return $this->recordLinkId;
    }

    /**
     * @param string $recordLinkId
     */
    public function setRecordLinkId(string $recordLinkId): void
    {
        $this->recordLinkId = $recordLinkId;
    }
}
