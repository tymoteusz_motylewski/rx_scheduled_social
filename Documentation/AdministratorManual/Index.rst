﻿.. include:: ../Includes.txt


.. _admin-manual:

Administrator Manual
====================


Installation
------------

Install the extension via the Extension Manager or composer as usual.


Configuration
-------------

The extension requires a working linkhandler configuration for ext:news. See example in the documentation of ext:news.

If the scheduler is invoked via CLI (e.g. from a cron-job) it is necessary to define **a domain**,
which can be used by the scheduler task, when generating absolute links, i.e. when sending those to Twitter.

This can be done with the option :typoscript:`config.socialDomain` in the TS setup.

.. code-block:: typoscript

   config.socialDomain = https://example.com/

.. note::

   Internally the scheduler tasks use :typoscript:`config.socialDomain` as :typoscript:`config.absRefPrefix` value.


Accessing social networks
-------------------------

To access the API of the social networks, you need to retrieve access tokens.
The necessary steps are described below. The tokes are required in order to have working tasks.


Twitter
^^^^^^^

Steps to retrieve the necessary tokens to access Twitter:

#. Go to https://apps.twitter.com/.
#. Create a new app and fill in the necessary details.
#. On the app management page select the Tab "Keys and Access Tokens".
#. Press "Create my access tokens".
#. Copy the according values into the task configuration.


Facebook
^^^^^^^^

Steps to retrieve the necessary tokes to access Facebook:

#. Go to https://developers.facebook.com/apps, create an app (choose the www option).
#. The App Id and App Secret can be found on the App Dashboard.
#. Go to https://developers.facebook.com/tools/explorer/ and select the new app from the select box.
#. Click on "Get Token" and then select "Get user access token", select manage pages and authorize the app.
#. Click again on "Get Token" and select the page you want to post to.
#. Copy the token (this token is only valid for one hour and is called *short_lived_token* in the next step).
#. Use the browser to make a get request to the url (replace ``{}`` with the according information)
   ``https://graph.facebook.com/v2.7/oauth/access_token?grant_type=fb_exchange_token&client_id={app_id}&client_secret={app_secret}&fb_exchange_token={short_lived_token}``.
#. This token is valid for two months.
#. Copy the token from the last step to the graph explorer and request ``me/?fields=access_token`` to get a permanent token.
#. Go to https://developers.facebook.com/tools/debug/accesstoken to check if the value from the access_token field does not expire.
#. Copy the **App Id**, **App Secret** and the **permanent token** to the task configuration.


Scheduler Task Configuration
----------------------------

#. Select the Scheduler module and add a new task.
#. Select the class of the task (all available tasks are grouped under *rx_scheduled_social*).
#. Enter the scheduler information (run frequency/start/stop time). For more details read the :ref:`Scheduler documentation <scheduler:introduction>`.
#. Enter the login tokens, the necessary information depends on the site that is the target of the task.
#. Enter the identifier of the linkhandler configuration to use.
#. Enter task specific configuration options.


Publish News Task
^^^^^^^^^^^^^^^^^

.. container:: table-row

   Property
      News Detail PID

   Data type
      integer

   Description
      The page id with the news plugin that displays the detail page.

   Required / Example
      Yes / 1


.. container:: table-row

   Property
      News storage PID list

   Data type
      comma-separated list of uids

   Description
      If specified only news posts from the given storage ids (and children of the specified ids) are posted.

   Required / Example
      No / 5,6


.. container:: table-row

   Property
      Category ID list

   Data type
      comma-separated list of uids

   Description
      If specified only news posts from the given categories (and child categories of the specified ids) are posted.

   Required / Example
      No / 5,6


.. note::

  Sharing the same news post on two different accounts from the same platform is currently not supported


Fetch Likes Task
^^^^^^^^^^^^^^^^

.. container:: table-row

   Property
      Facebook Page ID

   Data type
      integer

   Description
      The page id of the Facebook Page (can be found on the internal information section of the page).

   Required / Example
      Yes / 1



Fetch Followers Task
^^^^^^^^^^^^^^^^^^^^

.. container:: table-row

   Property
      Twitter Screen Name

   Data type
      string

   Description
      The screen name of the Twitter account.

   Required / Example
      Yes

