﻿.. include:: ../Includes.txt


.. _user-manual:

User manual
===========

First off: This extension does not provide any plugins, but works entirely in the background.

If the "fetch likes/follower" tasks are used, a Fluid view helper may be used to output acquired statistics in the frontend.
The type argument of the view helper must be :html:`facebook` or :html:`twitter`.
The id then specifies the screen name (twitter) or page id (facebook) that has been configured in the scheduler task,
which fetches the numbers.

In the following example two buttons for facebook and twitter are generated.

.. code-block:: html

  <html xmlns:f="http://typo3.org/ns/TYPO3/CMS/Fluid/ViewHelpers"
        xmlns:rx="http://typo3.org/ns/Reelworx/RxScheduledSocial/ViewHelpers"
        data-namespace-typo3-fluid="true"
  >
    <a class="btn btn-t" href="https://twitter.com/<youraccount>" target="_blank">
      <i class="fab fa-twitter" aria-hidden="true"></i>
      <span class="hidden-xs">Follow @<youraccount></span>
      <span class="badge">
        <aw:socialStatistics type="twitter" id="<youraccount>" />
      </span>
    </a>
    <a class="btn btn-f" href="https://www.facebook.com/<youraccount>/">
      <i class="fab fa-facebook" aria-hidden="true"></i>
      <span class="hidden-xs">We are on Facebook</span>
      <span class="badge">
        <aw:socialStatistics type="facebook" id="123" />
      </span>
    </a>
  </html>

.. image:: ../Images/UserManual/btns.png
