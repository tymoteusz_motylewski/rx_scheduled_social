﻿.. include:: ../Includes.txt


Change Log
==========

Version 2.0.0
-------------

* Added support for TYPO3 version 9.5
* Dropped support for TYPO3 version 7.6
* Removed Facebook publishing capabilities
* Improved documentation

Version 1.0.1
-------------

Allow TYPO3 version 8.7 and news version up to 7


Version 1.0.0
-------------

Initial release
