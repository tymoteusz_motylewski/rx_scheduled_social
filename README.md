Scheduled Social
================

This extension provides two functionalities:
 - automatically publish news posts (ext:news) to Twitter
 - fetch number of likes of your Twitter account or Facebook page 

Specifically fetching the number of likes is benefitial as no external JavaScript
solution is necessary. Therefore it prevents worries regarding GDPR.

Technically this is done with the help of scheduler tasks.
For more information please consider the extension manual https://docs.typo3.org/typo3cms/extensions/rx_scheduled_social/
